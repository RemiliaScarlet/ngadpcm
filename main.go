// Go port of https://github.com/superctr/adpcm/blob/master/yma_codec.c
//
// Original is by Ian Karlsson 2019.
// Go port by Remilia Scarlet, 2019
//
// Public domain

package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"io/ioutil"
	"math"
	"os"

	"scarletdevilmansion.tech/go/argparser-go"
	"scarletdevilmansion.tech/go/p36lib-go"
)

/*==============================================================================
 * ADPCM Codec
 *============================================================================*/

var stepTable = [49]uint16{
	16, 17, 19, 21, 23, 25, 28, 31, 34, 37, 41, 45, 50, 55, 60, 66, 73, 80, 88,
	97, 107,118,130,143, 157, 173, 190, 209, 230, 253, 279, 307, 337, 371, 408,
	449, 494, 544, 598, 658, 724, 796, 876, 963, 1060, 1166, 1282, 1411, 1552,
}

var stepAdjust = [8]int8{
	-1, -1, -1, -1, 2,  5,  7,  9,
}

var deltas = [16]int8{
	 1,  3,  5,  7,  9,  11,  13,  15,
	-1, -3, -5, -7, -9, -11, -13, -15,
}

type Codec struct {
	history  int16
	stepHist uint8
	nib      uint8
}

func (obj *Codec) doEncStep(input int16) uint8 {
	stepSize := uint16(stepTable[obj.stepHist])
	delta := input - obj.history
	samp := uint8(0)

	if delta < 0 {
		samp = 8
	}

	delta = int16(math.Abs(float64(delta)))

	bit := 3
	for bit != 0 {
		bit--

		if delta >= int16(stepSize) {
			samp |= (1 << bit)
			delta -= int16(stepSize)
		}

		stepSize = stepSize >> 1
	}

	obj.doStep(samp)

	return samp
}

func (obj *Codec) doStep(step uint8) int16 {
	var stepSize uint16
	var delta    int16
	var ret      int16
	var newStep  int8

	stepSize = stepTable[obj.stepHist]
	delta = int16(deltas[step & 15]) * int16(stepSize) / 8
	ret = (obj.history + delta) & 0xfff

	if ret & 0x800 != 0 {
		ret |= ^0xfff
	}

	obj.history = ret
	newStep = int8(obj.stepHist) + stepAdjust[step & 7]

	if newStep > 48 {
		obj.stepHist = 48
	} else if newStep < 0 {
		obj.stepHist = 0
	} else {
		obj.stepHist = uint8(newStep)
	}

	return ret
}

func (obj *Codec) Encode(data []int16, out *bytes.Buffer) {
	obj.history = 0
	obj.stepHist = 0
	obj.nib = 0
	bufSamp := uint8(0)

	var sample int16
	var step uint8

	for  i := 0; i < len(data); i++ {
		sample = data[i]

		if sample < 0x7ff8 { // round up
			sample += 8
		}

		sample = sample >> 4

		step = obj.doEncStep(sample)

		if obj.nib != 0 {
			out.WriteByte(bufSamp | (step & 15))
			obj.nib = 0
		} else {
			bufSamp = (step & 15) << 4
			obj.nib = 1
		}
	}
}

func (obj *Codec) Decode(data []byte, out *bytes.Buffer) {
	var step int8
	obj.history = 0
	obj.stepHist = 0
	obj.nib = 0
	srcIdx := 0

	for i := 0; i < len(data) * 2; i++ {
		step = int8(data[srcIdx]) << obj.nib
		step = step >> 4

		if obj.nib == 0 {
			obj.nib = 4
		} else {
			srcIdx++
			obj.nib = 0
		}

		p36lib.WriteInt16LE(obj.doStep(uint8(step)) << 4, out)
	}
}

/*==============================================================================
 * Utilities and WAV stuff
 *============================================================================*/

func die(msg string, fmtArgs ...interface{}) {
	fmt.Fprintf(os.Stderr, msg, fmtArgs...)
	os.Exit(1)
}

func dieError(err error) {
	fmt.Fprintf(os.Stderr, "%v\n", err)
	os.Exit(1)
}

// This really could be cleaned up, yuck
func readWav(data []byte) []int16 {
	in := bytes.NewReader(data)

	//
	// Read the RIFF header
	//

	if str, err := p36lib.ReadString(in, 4); str != "RIFF" || err != nil {
		return nil
	}

	// We don't care about the size of the RIFF chunk
	if _, err := p36lib.ReadInt32LE(in); err != nil {
		return nil
	}

	//
	// Read first subchunk (the "fmt " subchunk)
	//

	if str, err := p36lib.ReadString(in, 4); str != "WAVE" || err != nil {
		return nil
	}

	// Yes, the space needs to be in the "fmt " string
	if str, err := p36lib.ReadString(in, 4); str != "fmt " || err != nil {
		return nil
	}

	if fmtSize, err := p36lib.ReadInt32LE(in); fmtSize != 16 || err != nil {
		return nil
	}

	// We only support linear PCM
	if format, err := p36lib.ReadInt16LE(in); format != 1 || err != nil {
		return nil
	}

	// We only support one channel
	if numChan, err := p36lib.ReadInt16LE(in); numChan != 1 || err != nil {
		return nil
	}

	// We only support a sample rate of 18.5 KHz
	if rate, err := p36lib.ReadInt32LE(in); rate != 18500 || err != nil {
		return nil
	}

	// sample rate * num channels * (bit depth / 8)
	if byteRate, err := p36lib.ReadInt32LE(in); byteRate != 37000 || err != nil {
		return nil
	}

	// num channels * (bit depth / 8)
	if align, err := p36lib.ReadInt16LE(in); align != 2 || err != nil {
		return nil
	}

	if bitDepth, err := p36lib.ReadInt16LE(in); bitDepth != 16 || err != nil {
		return nil
	}

	//
	// Read the PCM data
	//

	if str, err := p36lib.ReadString(in, 4); str != "data" || err != nil {
		return nil
	}

	if numSamps, err := p36lib.ReadInt32LE(in); err != nil {
		return nil
	} else {
		// num samples * num channels * (bit depth / 8)
		//
		// Which for us is always:
		// num samples * 1 * 2
		ret := make([]int16, numSamps * 2, numSamps * 2)

		for idx, _ := range(ret) {
			if val, err := p36lib.ReadInt16LE(in); err != nil {
				// ...maybe not a valid WAV after all?
				return nil
			} else {
				ret[idx] = val
			}
		}

		return ret
	}
}

func writeWav(data []byte, out io.Writer) {
	writeStr := func(str string) {
		if num, err := out.Write([]byte(str)); err != nil {
			dieError(err)
		} else if num != len(str) {
			die("Could not write out WAV header\n")
		}
	}

	writeInt32 := func(num int32) {
		if err := p36lib.WriteInt32LE(num, out); err != nil {
			dieError(err)
		}
	}

	writeInt16 := func(num int16) {
		if err := p36lib.WriteInt16LE(num, out); err != nil {
			dieError(err)
		}
	}

	writeStr("RIFF")
	writeInt32(36 + int32(len(data)))  // RIFF size

	writeStr("WAVE")
	writeStr("fmt ")
	writeInt32(16)        // "fmt " size
	writeInt16(1)         // Linear PCM
	writeInt16(1)         // num channels
	writeInt32(18500)     // Sample rate
	writeInt32(18500 * 2) // Byte rate
	writeInt16(2)         // Block align
	writeInt16(16)        // Bit depth

	writeStr("data")
	writeInt32(int32(len(data)))

	if num, err := out.Write(data); err != nil {
		dieError(err)
	} else if num != len(data) {
		die("Could not write PCM data to WAV file\n")
	}
}

/*==============================================================================
 * Main entry point
 *============================================================================*/

func main() {
	args := argparser.NewArgParser("ngencode", "0.1")
	args.AddArgument("file", 'f', argparser.AT_String, nil, "", "Input filename")
	args.AddArgument("outfile", 'o', argparser.AT_String, nil, "", "Output filename")
	args.AddArgument("encode", 'e', argparser.AT_Flag, nil, "", "Do an encode instead of a decode")
	args.AddArgument("raw", 'r', argparser.AT_Flag, nil, "",
		"When decoding, output raw PCM data.  This has no meaning when encoding.")

	args.PostHelpText = `
The input must be 18.5 KHz 16-bit mono.  This can be either raw PCM data or a
WAV file - the program will determine automatically.

When encoding, the output is always saved as raw ADPCM-A data.  When decoding,
the output will be a 18.5 KHz 16-bit mono WAV file, or raw PCM data of the same
format if --raw is passed.

Both the --file and --outfile arguments can take a hyphen (-) instead of a
filename.  When this is done, the program will read from/write to standard
input/output, respectively.
`

	if err := args.Parse(os.Args, true, true); err != nil {
		fmt.Fprintf(os.Stderr, "%v\n", err)
		os.Exit(1)
	}

	if !args.ArgCalledP("--file") {
		fmt.Fprintf(os.Stderr, "No file specified\n")
		os.Exit(1)
	}

	if !args.ArgCalledP("--outfile") {
		fmt.Fprintf(os.Stderr, "No output file specified\n")
		os.Exit(1)
	}

	var data []byte
	var err error

	// Reading from stdin?
	inFile := args.GetArgSure("--file").GetString()
	if inFile == "-" {
		data, err = ioutil.ReadAll(os.Stdin)
	} else {
		data, err = ioutil.ReadFile(inFile)
	}

	if err != nil {
		panic(err)
	}

	dec := &Codec{}
	result := &bytes.Buffer{}

	if !args.ArgCalledP("--encode") {
		result.Grow(len(data) * 2)
		dec.Decode(data, result)
	} else {
		var realData []int16

		// Try to read the input as a WAV first
		if realData = readWav(data); realData == nil {
			// Likely raw PCM
			fmt.Fprintf(os.Stderr, "Input data isn't a valid WAV, processing as raw PCM\n")

			// Not a wav, assume raw PCM data
			realData = make([]int16, len(data) / 2, len(data) / 2)
			reader := bytes.NewReader(data)
			binary.Read(reader, binary.LittleEndian, &realData)
		}

		result.Grow(len(realData))
		dec.Encode(realData, result)
	}

	outFile := args.GetArgSure("--outfile").GetString()

	// Write to stdout?
	if outFile == "-" {
		if !args.ArgCalledP("--encode") && args.ArgCalledP("--raw") {
			// Doing a decode and asking for raw PCM on stdout
			os.Stdout.Write(result.Bytes())
		} else {
			// Write WAV file to stdout
			writeWav(result.Bytes(), os.Stdout)
		}

	} else {
		// Write to file, not stdout
		outStr, err := os.OpenFile(outFile, os.O_CREATE | os.O_TRUNC | os.O_WRONLY, 0640)
		if err != nil {
			dieError(err)
		}
		defer outStr.Close()

		outBytes := result.Bytes()
		if !args.ArgCalledP("--encode") && args.ArgCalledP("--raw") {
			// Doing a decode and asking for raw PCM
			if num, err := outStr.Write(outBytes); err != nil {
				dieError(err)
			} else if num != len(outBytes) {
				die("Could not save raw PCM data\n")
			}
		} else {
			// Write WAV file
			writeWav(outBytes, outStr)
		}
	}
}
