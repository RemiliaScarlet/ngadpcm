## ngadpcm

A Go port of a YM2610 ADPCM-A codec: https://github.com/superctr/adpcm/blob/master/yma_codec.c

This is one of the two ADPCM formats used the NeoGeo.

### Building

Just do a `go get gitlab.com/RemiliaScarlet/ngadpcm` and you should be good to
go.

### Usage

```
ngadpcm [options]

General Options
================================================================================
--help       / -h   : Shows help information
--version    / -V   : Shows version and license information
--file x     / -f x : Input filename
--outfile x  / -o x : Output filename
--encode     / -e   : Do an encode instead of a decode
--raw        / -r   : When decoding, output raw PCM data.  This has no meaning
                      when encoding.

The input must be 18.5 KHz 16-bit mono.  This can be either raw PCM data or a
WAV file - the program will determine automatically.

When encoding, the output is always saved as raw ADPCM-A data.  When decoding,
the output will be a 18.5 KHz 16-bit mono WAV file, or raw PCM data of the same
format if --raw is passed.

Both the --file and --outfile arguments can take a hyphen (-) instead of a
filename.  When this is done, the program will read from/write to standard
input/output, respectively.
```

### Copyright

Ian Karlsson 2019, Remilia Scarlet 2019

Public domain
